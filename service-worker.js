var CACHE = 'cache-v1';

self.addEventListener('install', function (evt) {
    console.log('The service worker is being installed.');
    evt.waitUntil(caches.open(CACHE).then(function (cache) {
        cache.addAll([
            'index',
            './',
        ]);
    }));
});

self.addEventListener('fetch', (event) => {
    event.respondWith(async function () {
        const cache = await caches.open(CACHE);
        const cachedResponse = await cache.match(event.request);
        const networkResponsePromise = fetch(event.request);

        event.waitUntil(
            updateCache(event.request)
        );

        return cachedResponse || networkResponsePromise;
    }());
});


function fromCache(request) {
    return caches.open(CACHE).then(function (cache) {
        return cache.match(request);
    });
}

function updateCache(request) {
    return caches.open(CACHE).then(function (cache) {
        return fetch(request).then(function (response) {
            return cache.put(request, response.clone()).then(function () {
                return response;
            });
        });
    });
}