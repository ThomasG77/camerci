import fs from 'fs'
import fse from 'fs-extra/esm'

var data = await fse.readJson("data.json");
var csv = "X,Y,NOM\n";

for(var d in data["features"]){
    var feature = data["features"][d];
    csv+=feature["geometry"]["x"]+","+feature["geometry"]["y"]+",n°"+feature["attributes"]["NUMERO_PUB"]+" "+feature["attributes"]["NOM"].replace(",","")+"\n";
}

fse.outputFile("data.csv", csv);
console.log("Done!");